﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TaxManager.Common.Contracts;
using TaxManager.Services.Contracts;
using System.Linq;
using TaxManager.Services.Models;
using System;

namespace TaxManager.Services.Services
{
    /// <summary>
    /// The TaxService class exposes two methods GetTaxRateForZipCode and CalculateTaxForOrder.
    /// For tax calculations uses any class implementing the ITaxService interface.
    /// Any class that implements the ITaxService will work with the TaxService class
    /// For this example we are using the TaxJarCalculatorService concrete class that implements the ITaxService interface
    /// Ideally the CalculateTaxForOrder function would receive a complex object as an overload
    /// </summary>
    public class TaxService : ITaxService
    {
        #region Declarations

        private readonly IEnumerable<ITaxCalculatorService> taxCalculatorServices;

        #endregion Declarations

        #region Constructor

        public TaxService(IEnumerable<ITaxCalculatorService> taxCalculatorServices)
        {
            this.taxCalculatorServices = taxCalculatorServices;

        }

        #endregion Constructor

        #region Methods

        /// <summary>
        /// Calls the tax calculator service with a valid input and returns the taxes for an order
        /// </summary>
        /// <param name="customerId">A valid customer</param>
        /// <param name="fromZipCode">Origing Zip Code</param>
        /// <param name="fromState">Origing state</param>
        /// <param name="toZipCode">Destination Zip Code</param>
        /// <param name="toState">Destination state</param>
        /// <param name="orderAmount">Order amount with out shipping</param>
        /// <param name="orderShippingAmount">Order shipping amount</param>
        /// <returns>The taxes to be collected for the order</returns>
        public async Task<Result<decimal>> CalculateTaxForOrder(
            int customerId,
            string fromZipCode,
            string fromState,
            string toZipCode,
            string toState,
            decimal orderAmount,
            decimal orderShippingAmount)
        {
            var result = new Result<decimal>();
            var calculator = GetCalculator(customerId);

            if (customerId <= 0) return result.SetError("Invalid Customer Id");
            if (calculator == null) return result.SetError("Tax Calculator not available for customer");

            try
            {
                //Calling the implemented Tax Calculator
                var response = await calculator.CalculateTaxForOrder(
                    fromZipCode,
                    fromState,
                    toZipCode,
                    toState,
                    orderAmount,
                    orderShippingAmount);

                result.SetSuccess(response);
            }
            catch (Exception)
            {
                //to do add logging exception
                result.SetError("Error while retrieving taxes");
            }
          
            return result;
        }

        /// <summary>
        /// Calls the tax calculator service with a valid zip code and returns a tax rate for the given zip code
        /// </summary>
        /// <param name="customerId">A valid customerId</param>
        /// <param name="postalCode">A valid zip code</param>
        /// <returns></returns>
        public async Task<Result<decimal>> GetTaxRateForZipCode(int customerId, string zipCode)
        {
            //Calling the implemented Tax Calculator
            var result = new Result<decimal>();
            var calculator = GetCalculator(customerId);

            if (customerId <= 0) return result.SetError("Invalid Customer Id");
            if (calculator == null) return result.SetError("Tax Calculator not available for customer");

            try
            {
                var response = await calculator.GetTaxForZipCode(zipCode);
                result.SetSuccess(response);
            }
            catch (Exception)
            {
                //to do add logging exception
                result.SetError("Error while retrieving taxes");
            }
           
            return result;
        }

        /// <summary>
        /// Getting the right Tax calculator for a given Customer Id
        /// </summary>
        /// <param name="customerId">A valid Customer Id</param>
        /// <returns>Tax Calculator Service for the selected customer</returns>
        private ITaxCalculatorService GetCalculator(int customerId)
        {
            //Depending on the customer's database settings we will use a different tax calculator
            //Select the TaxCalculatorId from the customers table where customerId equals the customerId param
            var TaxCalculatorId = "TaxJar";

            //A separate tax calculator provider maybe more appropiate
            //For simplicity since we only have taxjar implemented it is hard coded
            return this.taxCalculatorServices.FirstOrDefault(t => t.Id == TaxCalculatorId);
        }

        #endregion Methods
    }
}
