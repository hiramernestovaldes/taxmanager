﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxManager.Services.Models
{
    public class Result<T>
    {
        public bool Error { get; set; }
        public string Message { get; set; }
        public T Data { get; set; }
        public Result<T> SetError(string message)
        {
            this.Error = true;
            this.Message = message;
            return this;
        }
        public Result<T> SetSuccess(T data)
        {
            this.Error = false;
            this.Data = data;

            return this;
        }
    }
}
