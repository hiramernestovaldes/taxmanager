﻿using System.Threading.Tasks;
using TaxManager.Services.Models;

namespace TaxManager.Services.Contracts
{
    /// <summary>
    /// This is the interface for the Tax Service
    /// This is the public interface for Tax calculation abstracting the real calculator implementatiom
    /// </summary>
    public interface ITaxService
    {
        Task<Result<decimal>> CalculateTaxForOrder(
            int CustomerId,
            string fromZipCode,
            string fromState,
            string toZipCode,
            string toState,
            decimal orderAmount,
            decimal orderShippingAmount);

        Task<Result<decimal>> GetTaxRateForZipCode(int CustomerId, string zipCode);
    }
}
