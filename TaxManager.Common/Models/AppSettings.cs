﻿using TaxManager.Common.Contracts;

namespace TaxManager.Common.Models
{
    /// <summary>
    /// Holds the configuration for the services
    /// </summary>
    public class AppSettings : IAppSettings
    {
        public string TaxCalculationUrl { get; set; }
        public string TaxZipRateUrl { get; set; }
        public string ApiKey { get; set; }
    }
}
