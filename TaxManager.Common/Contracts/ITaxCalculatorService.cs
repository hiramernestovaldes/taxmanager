﻿using System;
using System.Threading.Tasks;

namespace TaxManager.Common.Contracts
{
    /// <summary>
    /// This is the interface for the Tax Calculator Service, allowing to be mocked for testing
    /// Any class implementing this interface will work with the Tax Service class
    /// </summary>
    public interface ITaxCalculatorService
    {
        string Id { get; }

        Task<decimal> CalculateTaxForOrder(
            string fromZipCode,
            string fromState,
            string toZipCode,
            string toState,
            decimal orderAmount,
            decimal orderShippingAmount);

        Task<decimal> GetTaxForZipCode(string zipCode);
    }
}
