﻿namespace TaxManager.Common.Contracts
{
    /// <summary>
    /// This the interface for the App settings
    /// Allowing to be mocked for testing
    /// </summary>
    public interface IAppSettings
    {
        string TaxCalculationUrl { get; set; }
        string TaxZipRateUrl { get; set; }
        string ApiKey { get; set; }
    }
}
