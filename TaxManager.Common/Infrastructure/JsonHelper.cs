﻿using System.Text.Json;

namespace System
{
    /// <summary>
    /// Json Helper, Serialization and Deserialization
    /// ToJson and FromJson should be accessible in all projects
    /// </summary>
    public static class JsonHelper
    {

        /// <summary>
        /// Setting serialization Options
        /// </summary>
        public static readonly JsonSerializerOptions options = new JsonSerializerOptions()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            WriteIndented = true,
            IgnoreNullValues = true
        };

        /// <summary>
        /// Serializes an object to json
        /// </summary>
        public static string ToJson(this object obj)
        {
            string result = JsonSerializer.Serialize(obj, options);

            return result;
        }

        /// <summary>
        /// Converts a json into an object
        /// </summary>
        public static T FromJson<T>(this string json)
        {
            return JsonSerializer.Deserialize<T>(json, options);
        }

    }
}