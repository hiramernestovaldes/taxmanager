using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Windows.Forms;
using TaxManager.Common.Contracts;
using TaxManager.Common.Models;
using TaxManager.Services.Contracts;
using TaxManager.Services.Services;
using TaxManager.Services.TaxJar.Services;

namespace TaxManager.UI
{
    /// <summary>
    /// The tax manager app uses dependency injection to provide the tax calculating logic
    /// There are two main functions in the TaxService class GetTaxRateForZipCode and CalculateTaxForOrder.
    /// The TaxService class is using the TaxJarTaxCalculatorService class through an implementation of the ITaxCalculatorService interface.
    /// Any other "Tax Calculator Service" that implements the ITaxCalculatorService interface will work with the TaxService class.
    /// </summary>
    static class Program
    {
        #region Declarations

        private static ServiceProvider serviceProvider;

        #endregion Declarations

        #region Main

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                //Winforms required settings
                Application.SetHighDpiMode(HighDpiMode.SystemAware);
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                //Configures dependency injection and settings for the  app
                Configure();

                //Getting main window from service provider
                var mainForm = serviceProvider.GetService<frmMain>();

                //Running the app
                Application.Run(mainForm);
            }
            catch (Exception ex)
            {
                //Any exceptions trying to load the app
                MessageBox.Show(ex.Message, "Tax Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
               //Disposing the dependency injection service provider if needed regardles of errors loading the app
               serviceProvider?.Dispose();
            }
        }

        #endregion Main

        #region Methods

        /// <summary>
        /// Configures dependency injection and settings for the app
        /// </summary>
        private static void Configure()
        {
            //Getting and loading app settings into a concrete class
            var appSettingsJson = File.ReadAllText("AppSettings.json");
            var appSettings = appSettingsJson.FromJson<AppSettings>();

            //Setting the dependency injectons, using TaxJarTaxCalculatorService concrete ITaxCalculatorService implementation
            var serviceCollection = new ServiceCollection()
                               .AddSingleton<IAppSettings>(appSettings)
                               .AddSingleton<ITaxCalculatorService, TaxCalculatorService>()
                               .AddSingleton<ITaxService, TaxService>()
                               .AddSingleton<frmMain>();

            //If we had more Tax calculator implementations we could add them here
            //A Tax calculator Provider would be better if the solution scales
            //serviceCollection.AddSingleton(ITaxCalculatorService, AnotherTaxCalculator);
            //serviceCollection.AddSingleton(ITaxCalculatorService, ThirdTaxCalculator);

            //Setting the dependency injecton provider
            serviceProvider = serviceCollection.BuildServiceProvider();
        }

        #endregion Methods
    }
}
