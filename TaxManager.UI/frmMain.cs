﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaxManager.Services.Contracts;

namespace TaxManager.UI
{
    /// <summary>
    /// Main UI for the Application
    /// </summary>
    public partial class frmMain : Form
    {
        #region Declarations

        private readonly ITaxService taxService;

        private const string APP_NAME = "Tax Manager";

        private const int CUSTOMER_ID = 1000;

        #endregion Declarations

        #region Constructors

        /// <summary>
        /// The form receives a tax service instance through dependency injection
        /// </summary>
        /// <param name="taxService">The taxService class is responsible for all the tax calculating logic</param>
        public frmMain(
            ITaxService taxService)
        {
            InitializeComponent();

            this.taxService = taxService;
        }

        #endregion Constructors

        #region Events

        private async void btnShowRate_Click(object sender, EventArgs e)
        {
           await ShowRate();
        }

        private async void btnCalculate_Click(object sender, EventArgs e)
        {
           await CalculateTaxes();
        }

        private void lblZipCode_DoubleClick(object sender, EventArgs e)
        {
            //just for rapid testing
            this.txtZipCode.Text = "33186";
            this.txtFromZip.Text = "33186";
            this.txtFromState.Text = "FL";
            this.txtToZip.Text = "33027";
            this.txtToState.Text = "FL";
            this.txtOrderAmount.Text = "100.00";
            this.txtOrderShippingAmount.Text = "5.55";
        }

        #endregion Events

        #region Methods

        /// <summary>
        /// Shows the tax rate for a given valid zip code
        /// If the zip code is invalid or there are other issues an error message will be shown
        /// </summary>
        private async Task ShowRate()
        {
            ShowProgress(true);

            try
            {
                var zipCode = this.txtZipCode.Text.Trim();

                //Calls the tax service GetTaxRateForZipCode function with a valid zip code and returns a tax rate for the given zip code
                var result = await this.taxService.GetTaxRateForZipCode(CUSTOMER_ID, zipCode);

                if(result.Error)
                {
                    MessageBox.Show(result.Message, APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show($"The tax rate for zip code {zipCode} is {result.Data}", APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                ShowProgress(false);
            }
        }

        /// <summary>
        /// Calculates taxes for an order given valid input data
        /// If the data is invalid or there are other issues an error message will be shown
        /// </summary>
        private async Task CalculateTaxes()
        {
            ShowProgress(true);

            //Simplistic error validation error handling
            string errorMessage = string.Empty;

            try
            {
                
                errorMessage = "Invalid order amount format";
                var orderAmount = Convert.ToDecimal(this.txtOrderAmount.Text.Trim());

                errorMessage = "Invalid order shipping amount format";
                var orderShippingAmount = Convert.ToDecimal(this.txtOrderShippingAmount.Text.Trim());

                errorMessage = $"Calculating taxes for order";
                //Calls the tax service CalculateTaxForOrder function with a valid input and returns the taxes for an order
                var result = await this.taxService.CalculateTaxForOrder(
                    CUSTOMER_ID,
                    this.txtFromZip.Text.Trim(),
                    this.txtFromState.Text.Trim(),
                    this.txtToZip.Text.Trim(),
                    this.txtToState.Text.Trim(), 
                    orderAmount, 
                    orderShippingAmount);

                if (result.Error)
                {
                    MessageBox.Show(result.Message, APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show($"The collectable tax amount is ${result.Data}", APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(errorMessage + System.Environment.NewLine + ex.Message, APP_NAME, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                ShowProgress(false);
            }
        }

        /// <summary>
        /// Shows/Hide progress while app is calculating taxes
        /// </summary>
        /// <param name="isBusy">When the App is busy</param>
        private void ShowProgress(bool isBusy)
        {
            this.Cursor = isBusy ? Cursors.WaitCursor : Cursors.Default;
            this.btnShowRate.Enabled = !isBusy;
            this.btnCalculate.Enabled = !isBusy;
        }

        #endregion Methods
    }
}
