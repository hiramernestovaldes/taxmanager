﻿
namespace TaxManager.UI
{
    partial class frmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalculate = new System.Windows.Forms.Button();
            this.txtToState = new System.Windows.Forms.TextBox();
            this.txtToZip = new System.Windows.Forms.TextBox();
            this.txtFromState = new System.Windows.Forms.TextBox();
            this.txtFromZip = new System.Windows.Forms.TextBox();
            this.txtOrderAmount = new System.Windows.Forms.TextBox();
            this.txtOrderShippingAmount = new System.Windows.Forms.TextBox();
            this.lblOrderShippingAmount = new System.Windows.Forms.Label();
            this.lblOrderAmount = new System.Windows.Forms.Label();
            this.lblToState = new System.Windows.Forms.Label();
            this.lblToZip = new System.Windows.Forms.Label();
            this.lblFromState = new System.Windows.Forms.Label();
            this.lblFromZip = new System.Windows.Forms.Label();
            this.txtZipCode = new System.Windows.Forms.TextBox();
            this.lblZipCode = new System.Windows.Forms.Label();
            this.btnShowRate = new System.Windows.Forms.Button();
            this.gbTaxes = new System.Windows.Forms.GroupBox();
            this.gbRates = new System.Windows.Forms.GroupBox();
            this.gbTaxes.SuspendLayout();
            this.gbRates.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(197, 218);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(75, 23);
            this.btnCalculate.TabIndex = 13;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // txtToState
            // 
            this.txtToState.Location = new System.Drawing.Point(165, 122);
            this.txtToState.MaxLength = 2;
            this.txtToState.Name = "txtToState";
            this.txtToState.Size = new System.Drawing.Size(107, 23);
            this.txtToState.TabIndex = 12;
            // 
            // txtToZip
            // 
            this.txtToZip.Location = new System.Drawing.Point(165, 93);
            this.txtToZip.MaxLength = 5;
            this.txtToZip.Name = "txtToZip";
            this.txtToZip.Size = new System.Drawing.Size(107, 23);
            this.txtToZip.TabIndex = 11;
            // 
            // txtFromState
            // 
            this.txtFromState.Location = new System.Drawing.Point(165, 63);
            this.txtFromState.MaxLength = 2;
            this.txtFromState.Name = "txtFromState";
            this.txtFromState.Size = new System.Drawing.Size(107, 23);
            this.txtFromState.TabIndex = 10;
            // 
            // txtFromZip
            // 
            this.txtFromZip.Location = new System.Drawing.Point(165, 33);
            this.txtFromZip.MaxLength = 5;
            this.txtFromZip.Name = "txtFromZip";
            this.txtFromZip.Size = new System.Drawing.Size(107, 23);
            this.txtFromZip.TabIndex = 2;
            // 
            // txtOrderAmount
            // 
            this.txtOrderAmount.Location = new System.Drawing.Point(165, 153);
            this.txtOrderAmount.MaxLength = 1000000;
            this.txtOrderAmount.Name = "txtOrderAmount";
            this.txtOrderAmount.Size = new System.Drawing.Size(107, 23);
            this.txtOrderAmount.TabIndex = 9;
            this.txtOrderAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtOrderShippingAmount
            // 
            this.txtOrderShippingAmount.Location = new System.Drawing.Point(165, 183);
            this.txtOrderShippingAmount.MaxLength = 1000000;
            this.txtOrderShippingAmount.Name = "txtOrderShippingAmount";
            this.txtOrderShippingAmount.Size = new System.Drawing.Size(107, 23);
            this.txtOrderShippingAmount.TabIndex = 8;
            this.txtOrderShippingAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblOrderShippingAmount
            // 
            this.lblOrderShippingAmount.AutoSize = true;
            this.lblOrderShippingAmount.Location = new System.Drawing.Point(6, 183);
            this.lblOrderShippingAmount.Name = "lblOrderShippingAmount";
            this.lblOrderShippingAmount.Size = new System.Drawing.Size(141, 15);
            this.lblOrderShippingAmount.TabIndex = 7;
            this.lblOrderShippingAmount.Text = "Order shippping amount:";
            // 
            // lblOrderAmount
            // 
            this.lblOrderAmount.AutoSize = true;
            this.lblOrderAmount.Location = new System.Drawing.Point(6, 153);
            this.lblOrderAmount.Name = "lblOrderAmount";
            this.lblOrderAmount.Size = new System.Drawing.Size(85, 15);
            this.lblOrderAmount.TabIndex = 6;
            this.lblOrderAmount.Text = "Order amount:";
            // 
            // lblToState
            // 
            this.lblToState.AutoSize = true;
            this.lblToState.Location = new System.Drawing.Point(6, 123);
            this.lblToState.Name = "lblToState";
            this.lblToState.Size = new System.Drawing.Size(50, 15);
            this.lblToState.TabIndex = 5;
            this.lblToState.Text = "To state:";
            // 
            // lblToZip
            // 
            this.lblToZip.AutoSize = true;
            this.lblToZip.Location = new System.Drawing.Point(6, 93);
            this.lblToZip.Name = "lblToZip";
            this.lblToZip.Size = new System.Drawing.Size(69, 15);
            this.lblToZip.TabIndex = 4;
            this.lblToZip.Text = "To zip code:";
            // 
            // lblFromState
            // 
            this.lblFromState.AutoSize = true;
            this.lblFromState.Location = new System.Drawing.Point(6, 63);
            this.lblFromState.Name = "lblFromState";
            this.lblFromState.Size = new System.Drawing.Size(66, 15);
            this.lblFromState.TabIndex = 3;
            this.lblFromState.Text = "From state:";
            // 
            // lblFromZip
            // 
            this.lblFromZip.AutoSize = true;
            this.lblFromZip.Location = new System.Drawing.Point(6, 33);
            this.lblFromZip.Name = "lblFromZip";
            this.lblFromZip.Size = new System.Drawing.Size(85, 15);
            this.lblFromZip.TabIndex = 2;
            this.lblFromZip.Text = "From zip code:";
            // 
            // txtZipCode
            // 
            this.txtZipCode.Location = new System.Drawing.Point(165, 32);
            this.txtZipCode.MaxLength = 5;
            this.txtZipCode.Name = "txtZipCode";
            this.txtZipCode.Size = new System.Drawing.Size(107, 23);
            this.txtZipCode.TabIndex = 1;
            // 
            // lblZipCode
            // 
            this.lblZipCode.AutoSize = true;
            this.lblZipCode.Location = new System.Drawing.Point(6, 32);
            this.lblZipCode.Name = "lblZipCode";
            this.lblZipCode.Size = new System.Drawing.Size(56, 15);
            this.lblZipCode.TabIndex = 1;
            this.lblZipCode.Text = "Zip code:";
            this.lblZipCode.DoubleClick += new System.EventHandler(this.lblZipCode_DoubleClick);
            // 
            // btnShowRate
            // 
            this.btnShowRate.Location = new System.Drawing.Point(197, 61);
            this.btnShowRate.Name = "btnShowRate";
            this.btnShowRate.Size = new System.Drawing.Size(75, 23);
            this.btnShowRate.TabIndex = 0;
            this.btnShowRate.Text = "Show Rate";
            this.btnShowRate.UseVisualStyleBackColor = true;
            this.btnShowRate.Click += new System.EventHandler(this.btnShowRate_Click);
            // 
            // gbTaxes
            // 
            this.gbTaxes.Controls.Add(this.btnCalculate);
            this.gbTaxes.Controls.Add(this.txtToState);
            this.gbTaxes.Controls.Add(this.txtToZip);
            this.gbTaxes.Controls.Add(this.txtFromState);
            this.gbTaxes.Controls.Add(this.txtFromZip);
            this.gbTaxes.Controls.Add(this.txtOrderAmount);
            this.gbTaxes.Controls.Add(this.txtOrderShippingAmount);
            this.gbTaxes.Controls.Add(this.lblOrderShippingAmount);
            this.gbTaxes.Controls.Add(this.lblOrderAmount);
            this.gbTaxes.Controls.Add(this.lblToState);
            this.gbTaxes.Controls.Add(this.lblToZip);
            this.gbTaxes.Controls.Add(this.lblFromState);
            this.gbTaxes.Controls.Add(this.lblFromZip);
            this.gbTaxes.Location = new System.Drawing.Point(12, 121);
            this.gbTaxes.Name = "gbTaxes";
            this.gbTaxes.Size = new System.Drawing.Size(278, 247);
            this.gbTaxes.TabIndex = 3;
            this.gbTaxes.TabStop = false;
            this.gbTaxes.Text = "Taxes for order";
            // 
            // gbRates
            // 
            this.gbRates.Controls.Add(this.txtZipCode);
            this.gbRates.Controls.Add(this.lblZipCode);
            this.gbRates.Controls.Add(this.btnShowRate);
            this.gbRates.Location = new System.Drawing.Point(12, 12);
            this.gbRates.Name = "gbRates";
            this.gbRates.Size = new System.Drawing.Size(278, 91);
            this.gbRates.TabIndex = 2;
            this.gbRates.TabStop = false;
            this.gbRates.Text = "Rates by zip code";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 377);
            this.Controls.Add(this.gbTaxes);
            this.Controls.Add(this.gbRates);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tax Manager for Customer 1000";
            this.gbTaxes.ResumeLayout(false);
            this.gbTaxes.PerformLayout();
            this.gbRates.ResumeLayout(false);
            this.gbRates.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.TextBox txtToState;
        private System.Windows.Forms.TextBox txtToZip;
        private System.Windows.Forms.TextBox txtFromState;
        private System.Windows.Forms.TextBox txtFromZip;
        private System.Windows.Forms.TextBox txtOrderAmount;
        private System.Windows.Forms.TextBox txtOrderShippingAmount;
        private System.Windows.Forms.Label lblOrderShippingAmount;
        private System.Windows.Forms.Label lblOrderAmount;
        private System.Windows.Forms.Label lblToState;
        private System.Windows.Forms.Label lblToZip;
        private System.Windows.Forms.Label lblFromState;
        private System.Windows.Forms.Label lblFromZip;
        private System.Windows.Forms.TextBox txtZipCode;
        private System.Windows.Forms.Label lblZipCode;
        private System.Windows.Forms.Button btnShowRate;
        private System.Windows.Forms.GroupBox gbTaxes;
        private System.Windows.Forms.GroupBox gbRates;
    }
}

