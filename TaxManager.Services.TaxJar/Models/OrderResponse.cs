﻿using Newtonsoft.Json;

namespace TaxManager.Services.TaxJar.Models
{
    /// <summary>
    /// Tax Jar specific Order response implementation
    /// </summary>
    public class OrderResponse
    {
        public Tax Tax { get; set; }
    }

    public class Tax
    {
        [JsonProperty("amount_to_collect")]
        public decimal AmountToCollect { get; set; }
    }
}
