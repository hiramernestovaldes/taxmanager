﻿using Newtonsoft.Json;

namespace TaxManager.Services.TaxJar.Models
{
    /// <summary>
    /// Tax Jar specific Order request implementation
    /// </summary>
    public class OrderRequest
    {

        [JsonProperty("from_zip")]
        public string FromZip { get; set; }

        [JsonProperty("from_state")]
        public string FromState { get; set; }

        [JsonProperty("to_zip")]
        public string ToZip { get; set; }

        [JsonProperty("to_state")]
        public string ToState { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("shipping")]
        public decimal ShippingAmount { get; set; }

    }
}
