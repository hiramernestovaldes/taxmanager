﻿using Newtonsoft.Json;

namespace TaxManager.Services.TaxJar.Models
{
    /// <summary>
    /// Tax Jar specific location response implementation
    /// </summary>
    public class ZipCodeResponse
    {
        public Rate Rate { get; set; }
    }

    public class Rate
    {
        [JsonProperty("combined_rate")]
        public decimal CombinedRate { get; set; }
    }
}
