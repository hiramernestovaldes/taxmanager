﻿using Flurl;
using Flurl.Http;
using System;
using System.Threading.Tasks;
using TaxManager.Common.Contracts;
using TaxManager.Services.TaxJar.Models;

namespace TaxManager.Services.TaxJar.Services
{
    /// <summary>
    /// Specific implementation of the ITaxCalculatorService interface
    /// This is a concrete Tax Jar API Proxy implementation
    /// Ideally the CalculateTaxForOrder function would receive a complex object as an overload
    /// </summary>
    public class TaxCalculatorService : ITaxCalculatorService
    {
        #region Declarations

        private readonly IAppSettings appSettings;

        #endregion Declarations

        #region Constructor

        /// <summary>
        /// The settings with the urls and access key
        /// </summary>
        /// <param name="appSettings">Application Settings</param>
        public TaxCalculatorService(IAppSettings appSettings)
        {
            this.appSettings = appSettings;
        }

        #endregion Constructor

        #region Properties

        public string Id { get; private set; } = "TaxJar";

        #endregion Properties

        #region Methods

        /// <summary>
        /// Calculate sales tax for an order
        /// </summary>
        /// <param name="fromZip">Postal code where the order shipped from (5-Digit)</param>
        /// <param name="fromState">Two-letter ISO state code where the order shipped from.</param>
        /// <param name="toZip">Postal code where the order shipped to (5-Digit).</param>
        /// <param name="toState">Two-letter ISO state code where the order shipped to.</param>
        /// <param name="orderAmount">Total amount of the order, excluding shipping.</param>
        /// <param name="orderShippingAmount">Total amount of shipping for the order.</param>
        /// <returns>Gets the sales tax that should be collected for a given order</returns>
        public async Task<decimal> CalculateTaxForOrder(
            string fromZip,
            string fromState,
            string toZip,
            string toState,
            decimal orderAmount,
            decimal orderShippingAmount)
        {
            //creating the request
            var request = new OrderRequest
            {
                FromZip = fromZip,
                FromState = fromState,
                ToZip = toZip,
                ToState = toState,
                Amount = orderAmount,
                ShippingAmount = orderShippingAmount
            };

            //Posting the request to the Tax Jar API and receiving the taxes for the order
            var response = await this.appSettings.TaxCalculationUrl
                .WithOAuthBearerToken(this.appSettings.ApiKey)
                .PostJsonAsync(request)
                .ReceiveJson<OrderResponse>();

            //returns the sales tax that should be collected for a given order
            //According to the sites documentation the amount_to_collect should be returned
            //amount_to_collect => Amount of sales tax to collect
            return response.Tax.AmountToCollect;
        }

        /// <summary>
        /// Gets tax rates for a given zipcode
        /// </summary>
        /// <param name="zipCode">Postal code for given location (5-Digit)</param>
        /// <returns>Gets the sales tax rates for a given location.</returns>
        public async Task<decimal> GetTaxForZipCode(string zipCode)
        { 
            //Getting the tax rate for a given zip code using the Tax Jar API
            var response = await this.appSettings.TaxZipRateUrl
                .AppendPathSegment(zipCode)
                .WithOAuthBearerToken(this.appSettings.ApiKey)
                .GetJsonAsync<ZipCodeResponse>();

            //Returns the sales tax rates for a given zip code.
            //According to the sites documentation the combined_rate should be returned
            //combined_district_rate => aggregates the rates for all city and county sales tax districts effective at the location.
            return response.Rate.CombinedRate;
        }

        #endregion Methods
    }
}
