﻿using Flurl.Http.Testing;
using System;
using System.Threading.Tasks;
using TaxManager.Common.Contracts;
using Xunit;
using Moq;
using FluentAssertions;
using System.Net.Http;
using TaxManager.Services.TaxJar.Services;
using Flurl.Http;

namespace TaxManager.Services.UnitTests
{
    /// <summary>
    /// Testing TaxJarCalculatorService class for positives and negatives outcomes.
    /// Tax Jar is a third party API and since this is a unit test, all the calls are faked and the responses mocked using the Flurl http testing framework.
    /// </summary>
    public class TaxJarServiceTests : IDisposable
    {
        #region Declarations

        private readonly HttpTest httpTest;
        private readonly Mock<IAppSettings> appSettingsMock;
        private readonly ITaxCalculatorService sut;

        protected const string TAX_CALCULATION_URL = "http://taxcalculationurl/fakeaddress";
        protected const string TAX_ZIPRATE_URL = "http://taxziprateurl/fakeaddress/";
        protected const string API_KEY = "FakeKey";
        private const string VALID_ZIPCODE = "33186";
        private const string INVALID_ZIPCODE = "1";
        protected const string VALID_STATE = "FL";
        protected const decimal VALID_ORDER_AMOUNT = 100M;
        protected const decimal VALID_ORDER_SHIPPING_AMOUNT = 10M;

        #endregion Declarations

        #region Constructor

        public TaxJarServiceTests()
        {
            //creates an instance of the Flurl http testing framework
            this.httpTest = new HttpTest();

            this.appSettingsMock = new Mock<IAppSettings>();
            this.appSettingsMock.SetupAllProperties();
            this.appSettingsMock.Object.TaxCalculationUrl = TAX_CALCULATION_URL;
            this.appSettingsMock.Object.TaxZipRateUrl = TAX_ZIPRATE_URL;
            this.appSettingsMock.Object.ApiKey = API_KEY;
            //this.appSettingsMock.Setup(a => a.TaxCalculationUrl).Returns(TAX_CALCULATION_URL);
            //this.appSettingsMock.Setup(a => a.TaxZipRateUrl).Returns(TAX_ZIPRATE_URL);
            //this.appSettingsMock.Setup(a => a.ApiKey).Returns(API_KEY);

            this.sut = new TaxCalculatorService(this.appSettingsMock.Object);
        }

        #endregion Constructor

        #region Get Rates

        /// <summary>
        /// Testing positive outcome for the GetTaxRateForZipCode function with a valid zip code
        /// </summary>
        [Fact]
        public async Task GetTaxRateForZipCode_WhenGivenValidZipcode_ShouldReturnTaxRate()
        {
            //Arrange       
            decimal expected = 0.07M;
            this.httpTest.RespondWithJson(new { rate = new { combined_rate = expected } });
           
            //Act
            var response = await this.sut.GetTaxForZipCode(VALID_ZIPCODE);

            //Assert
            response.Should().Be(expected);
            httpTest
                .ShouldHaveCalled(TAX_ZIPRATE_URL + VALID_ZIPCODE)
                .WithVerb(HttpMethod.Get)
                .WithOAuthBearerToken(API_KEY);
           
        }

        /// <summary>
        /// Testing many positive outcomes for the GetTaxRateForZipCode function with valid zip codes
        /// </summary>
        [Theory]
        [InlineData("85001", 0.086)]
        [InlineData("33027", 0.07)]
        [InlineData("10014", 0.08875)]
        [InlineData("33186", 0.07)]
        public async Task GetTaxRateForZipCode_WhenGivenValidZipcodes_ShouldReturnTaxRates(string zipCode, decimal expected)
        {
            //Arrange       
            httpTest.RespondWithJson(new { rate = new { combined_rate = expected } });

            //Act
            var response = await this.sut.GetTaxForZipCode(zipCode);

            //Assert
            response.Should().Be(expected);
            httpTest
                .ShouldHaveCalled(TAX_ZIPRATE_URL + zipCode)
                .WithVerb(HttpMethod.Get)
                .WithOAuthBearerToken(API_KEY);
           
        }

        /// <summary>
        /// Testing negative outcome for the GetTaxRateForZipCode function with invalid zip code
        /// </summary>
        [Fact]
        public void GetTaxRateForZipCode_WhenGivenInvalidZipcode_ShouldThrowFlurlHttpException()
        {
            //Arrange 
            var response = new { status = 404, error = "Not Found", detail = "Resource can not be found" };
            httpTest.RespondWith(response.ToJson(), 404);

            //Act and Assert
            this.sut
                .Invoking(async s => await s.GetTaxForZipCode(INVALID_ZIPCODE))
                .Should()
                .Throw<FlurlHttpException>();

            httpTest
                .ShouldHaveCalled(TAX_ZIPRATE_URL + INVALID_ZIPCODE)
                .WithVerb(HttpMethod.Get)
                .WithOAuthBearerToken(API_KEY);
        }

        #endregion

        #region Calculate Taxes

        /// <summary>
        /// Testing positive outcome for the CalculateTaxForOrder function with valid input data
        /// </summary>
        [Fact]
        public async Task CalculateTaxForOrder_ShouldReturnTaxAmount_GivenValidInput()
        {
            //Arrange       
            decimal expected = 7;
            httpTest.RespondWithJson(new { tax = new { amount_to_collect = expected } });
           
            //Act
            var response = await this.sut.CalculateTaxForOrder(
                VALID_ZIPCODE, 
                VALID_STATE, 
                VALID_ZIPCODE, 
                VALID_STATE, 
                VALID_ORDER_AMOUNT, 
                VALID_ORDER_SHIPPING_AMOUNT);

            //Assert
            response.Should().Be(expected);
            httpTest
                .ShouldHaveCalled(TAX_CALCULATION_URL)
                .WithVerb(HttpMethod.Post)
                .WithOAuthBearerToken(API_KEY);
        }

        /// <summary>
        /// Testing many positive outcomes for the CalculateTaxForOrder function with valid input data
        /// </summary>
        [Theory]
        [InlineData("33186", "FL", "33027", "FL", 100, 10, 7)]
        [InlineData("92093", "CA", "90002", "CA", 88.22, 2.56, 84.12)]
        [InlineData("10012", "NY", "10014", "NY", 789.34, 42.25, 55.24)]
        public async Task CalculateTaxForOrder_ShouldReturnTaxAmount_GivenManyValidInputs(
            string fromZipCode,
            string fromState,
            string ToZipCode,
            string toState,
            decimal orderAmount,
            decimal orderShippingAmount,
            decimal expected)
        {
            //Arrange       
            httpTest.RespondWithJson(new { tax = new { amount_to_collect = expected } });
            

            //Act
            var response = await this.sut.CalculateTaxForOrder(
                fromZipCode, 
                fromState, 
                ToZipCode, 
                toState, 
                orderAmount, 
                orderShippingAmount);

            //Assert
            response.Should().Be(expected);
            httpTest
                .ShouldHaveCalled(TAX_CALCULATION_URL)
                .WithVerb(HttpMethod.Post)
                .WithOAuthBearerToken(API_KEY);
        }

        /// <summary>
        /// Testing negative outcome for the CalculateTaxForOrder function with invalid input data
        /// </summary>
        [Fact]
        public void CalculateTaxForOrder_ShouldThrowFlurlHttpException_GivenInvalidInput()
        {
            //Arrange 
            var response = new { status = 404, error = "Not Found", detail = "Resource can not be found" };
            httpTest.RespondWith(response.ToJson(), 404);

            //Act and Assert
            this.sut
                .Invoking(async s => await s.CalculateTaxForOrder(
                    INVALID_ZIPCODE, 
                    VALID_STATE, 
                    INVALID_ZIPCODE, 
                    VALID_STATE, 
                    VALID_ORDER_AMOUNT, 
                    VALID_ORDER_SHIPPING_AMOUNT))
                .Should()
                .Throw<FlurlHttpException>();

            httpTest
               .ShouldHaveCalled(TAX_CALCULATION_URL)
               .WithVerb(HttpMethod.Post)
               .WithOAuthBearerToken(API_KEY);
        }

        #endregion Calculate Taxes

        #region Methods
        public void Dispose() => this.httpTest?.Dispose();
     
        #endregion Methods
    }
}
