using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaxManager.Common.Contracts;
using TaxManager.Services.Contracts;
using TaxManager.Services.Services;
using Xunit;

namespace TaxManager.Services.UnitTests
{
    /// <summary>
    /// Testing TaxService class for positives and negatives outcomes.
    /// Any other "Tax Calculator Service" that implements the ITaxCalculatorService interface will work with the TaxService class.
    /// The TaxCalculatorService is been mocked and fed to the sut in this case the TaxService class
    /// For mocking the MOQ framework is used.
    /// </summary>
    public class TaxServiceTests 
    {
        #region Declarations

        protected const string VALID_ZIP_CODE = "33186";
        protected const string VALID_STATE = "FL";
        protected const decimal VALID_ORDER_AMOUNT = 100M;
        protected const decimal VALID_ORDER_SHIPPING_AMOUNT = 10M;
        protected const string INVALID_ZIP_CODE = "1";
        protected const int VALID_CUSTOMER_ID = 1000;
        protected const int INVALID_CUSTOMER_ID = 0;
        protected const string TAX_CALCULATOR_NOT_AVAILABLE = "Tax Calculator not available for customer";
        protected const string VALID_TAX_CALCULATOR_ID = "TaxJar";
        protected const string INVALID_TAX_CALCULATOR_ID = "OtherTaxCalculator";
        protected const string TAX_SERVICE_EXCEPTION = "Error while retrieving taxes";
        protected const string INVALID_CUSTOMER_ID_EXCEPTION = "Invalid Customer Id";

        #endregion Declarations

        #region Get Rates

        /// <summary>
        /// Testing positive outcome for the GetTaxRateForZipCode function with a valid data
        /// </summary>
        [Fact]
        public async Task GetTaxRateForZipCode_WhenGivenValidZipcode_ShouldReturnTaxRate()
        {
            //Arrange
            decimal expected = 10.25M;
            var taxCalculatorServiceMock = new Mock<ITaxCalculatorService>();
            taxCalculatorServiceMock.Setup(x => x.Id).Returns(VALID_TAX_CALCULATOR_ID);
            taxCalculatorServiceMock.Setup(x => x.GetTaxForZipCode(It.IsAny<string>())).Returns(Task.FromResult(expected));
            var sut = new TaxService(new List<ITaxCalculatorService> { taxCalculatorServiceMock.Object });

            //Act            
            var response = await sut.GetTaxRateForZipCode(VALID_CUSTOMER_ID, VALID_ZIP_CODE);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeFalse();
            response.Data.Should().Be(expected);
        }

        /// <summary>
        /// Testing many positive outcomes for the GetTaxRateForZipCode function with valid zip codes
        /// </summary>
        [Theory]
        [InlineData("85001", 0.086)]
        [InlineData("33027", 0.07)]
        [InlineData("10014", 0.08875)]
        [InlineData("33186", 0.07)]
        public async Task GetTaxRateForZipCode_WhenGivenValidZipcodes_ShouldReturnTaxRates(string zipCode, decimal expected)
        {
            //Arrange
            var calculatorService = new Moq.Mock<ITaxCalculatorService>();
            calculatorService.Setup(x => x.Id).Returns(VALID_TAX_CALCULATOR_ID);
            calculatorService.Setup(x => x.GetTaxForZipCode(It.IsAny<string>())).Returns(Task.FromResult(expected));
            var sut = new TaxService(new List<ITaxCalculatorService> { calculatorService.Object });

            //Act            
            var response = await sut.GetTaxRateForZipCode(VALID_CUSTOMER_ID, zipCode);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeFalse();
            response.Data.Should().Be(expected);
        }

        /// <summary>
        /// Testing negative outcome for the GetTaxRateForZipCode exception
        /// </summary>
        [Fact]
        public async Task GetTaxRateForZipCode_WhenCalculatorThrowsException_ShouldReturnError()
        {
            //Arrange
            var taxCalculatorServiceMock = new Mock<ITaxCalculatorService>();
            taxCalculatorServiceMock.Setup(x => x.Id).Returns(VALID_TAX_CALCULATOR_ID);
            taxCalculatorServiceMock.Setup(x => x.GetTaxForZipCode(It.IsAny<string>())).Throws<Exception>();
            var sut = new TaxService(new List<ITaxCalculatorService> { taxCalculatorServiceMock.Object });

            //Act            
            var response = await sut.GetTaxRateForZipCode(VALID_CUSTOMER_ID, INVALID_ZIP_CODE);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeTrue();
            response.Message.Should().Be(TAX_SERVICE_EXCEPTION);
        }

        /// <summary>
        /// Testing negative outcome given an invalid customer id
        /// </summary>
        [Fact]
        public async Task GetTaxRateForZipCode_WhenGivenInvalidCustomerId_ShouldReturnError()
        {
            //Arrange
            var taxCalculatorServiceMock = new Mock<ITaxCalculatorService>();
            taxCalculatorServiceMock.Setup(x => x.Id).Returns(VALID_TAX_CALCULATOR_ID);
           // taxCalculatorServiceMock.Setup(x => x.GetTaxForZipCode(It.IsAny<string>())).Throws<Exception>();
            var sut = new TaxService(new List<ITaxCalculatorService> { taxCalculatorServiceMock.Object });

            //Act            
            var response = await sut.GetTaxRateForZipCode(INVALID_CUSTOMER_ID, VALID_ZIP_CODE);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeTrue();
            response.Message.Should().Be(INVALID_CUSTOMER_ID_EXCEPTION);
        }

        /// <summary>
        /// Testing negative outcome when the customers Tax calculator is not found
        /// </summary>
        [Fact]
        public async Task GetTaxRateForZipCode_WhenTaxJarCalculatorIsNotAvailable_ShouldReturnError()
        {
            //Arrange
            var taxCalculatorServiceMock = new Mock<ITaxCalculatorService>();
            taxCalculatorServiceMock.Setup(x => x.Id).Returns(INVALID_TAX_CALCULATOR_ID);
            //taxCalculatorServiceMock.Setup(x => x.GetTaxForZipCode(It.IsAny<string>())).Throws<Exception>();
            var sut = new TaxService(new List<ITaxCalculatorService> { taxCalculatorServiceMock.Object });

            //Act            
            var response = await sut.GetTaxRateForZipCode(VALID_CUSTOMER_ID, VALID_ZIP_CODE);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeTrue();
            response.Message.Should().Be(TAX_CALCULATOR_NOT_AVAILABLE);
        }

        #endregion Get Rates

        #region Calculate Taxes

        /// <summary>
        /// Testing positive outcome for the CalculateTaxForOrder function with valid input data
        /// </summary>
        [Fact]
        public async Task CalculateTaxForOrder_WhenGivenValidData_ShouldReturnTaxAmount()
        {
            //Arrange
            decimal expected = 7;
            var taxCalculatorServiceMock = new Moq.Mock<ITaxCalculatorService>();
            taxCalculatorServiceMock.Setup(x => x.Id).Returns(VALID_TAX_CALCULATOR_ID);
            taxCalculatorServiceMock.Setup(x => x.CalculateTaxForOrder(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<decimal>(),
                It.IsAny<decimal>()))
                .Returns(Task.FromResult(expected));
            var sut = new TaxService(new List<ITaxCalculatorService> { taxCalculatorServiceMock.Object });

            //Act            
            var response = await sut.CalculateTaxForOrder(
                VALID_CUSTOMER_ID, 
                VALID_ZIP_CODE,
                VALID_STATE,
                VALID_ZIP_CODE,
                VALID_STATE,
                VALID_ORDER_AMOUNT,
                VALID_ORDER_SHIPPING_AMOUNT);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeFalse();
            response.Data.Should().Be(expected);
        }

        /// <summary>
        /// Testing many positive outcomes for the CalculateTaxForOrder function with valid input data
        /// </summary>
        [Theory]
        [InlineData("33186", "FL", "33027", "FL", 100, 10, 7)]
        [InlineData("92093", "CA", "90002", "CA", 88.22, 2.56, 84.12)]
        [InlineData("10012", "NY", "10014", "NY", 789.34, 42.25, 55.24)]
        public async Task CalculateTaxForOrder_WhenGivenValidDataSet_ShouldReturnTaxAmounts(
            string fromZipCode,
            string fromState,
            string ToZipCode,
            string toState,
            decimal orderAmount,
            decimal orderShippingAmount,
            decimal expected)
        {
            var calculatorService = new Moq.Mock<ITaxCalculatorService>();
            calculatorService.Setup(x => x.Id).Returns(VALID_TAX_CALCULATOR_ID);
            calculatorService.Setup(x => x.CalculateTaxForOrder(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<decimal>(),
                It.IsAny<decimal>()))
                .Returns(Task.FromResult(expected));
            var sut = new TaxService(new List<ITaxCalculatorService> { calculatorService.Object });

            //Act            
            var response = await sut.CalculateTaxForOrder(
                VALID_CUSTOMER_ID, 
                fromZipCode, 
                fromState, 
                ToZipCode, 
                toState, 
                orderAmount, 
                orderShippingAmount);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeFalse();
            response.Data.Should().Be(expected);
        }

        /// <summary>
        /// Testing negative outcome for the CalculateTaxForOrder Exception
        /// </summary>
        [Fact]
        public async Task CalculateTaxForOrder_WhenCalculatorThrowsException_ShouldReturnError()
        {
            //Arrange
            var taxCalculatorServiceMock = new Moq.Mock<ITaxCalculatorService>();
            taxCalculatorServiceMock.Setup(x => x.Id).Returns(VALID_TAX_CALCULATOR_ID);
            taxCalculatorServiceMock.Setup(x => x.CalculateTaxForOrder(
               It.IsAny<string>(),
               It.IsAny<string>(),
               It.IsAny<string>(),
               It.IsAny<string>(),
               It.IsAny<decimal>(),
               It.IsAny<decimal>()))
               .Throws<Exception>();
            var sut = new TaxService(new List<ITaxCalculatorService> { taxCalculatorServiceMock.Object });

            //Act                       
            var response = await sut.CalculateTaxForOrder(
                VALID_CUSTOMER_ID,
                INVALID_ZIP_CODE,
                VALID_STATE,
                INVALID_ZIP_CODE,
                VALID_STATE,
                VALID_ORDER_AMOUNT,
                VALID_ORDER_SHIPPING_AMOUNT);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeTrue();
            response.Message.Should().Be(TAX_SERVICE_EXCEPTION);
        }

        /// <summary>
        /// Testing negative outcome when the customers Tax calculator is not found
        /// </summary>
        [Fact]
        public async Task CalculateTaxForOrder_WhenTaxJarCalculatorIsNotAvailable_ShouldReturnError()
        {
            //Arrange
            var taxCalculatorServiceMock = new Moq.Mock<ITaxCalculatorService>();
            taxCalculatorServiceMock.Setup(x => x.Id).Returns(INVALID_TAX_CALCULATOR_ID);
            //taxCalculatorServiceMock.Setup(x => x.CalculateTaxForOrder(
            //   It.IsAny<string>(),
            //   It.IsAny<string>(),
            //   It.IsAny<string>(),
            //   It.IsAny<string>(),
            //   It.IsAny<decimal>(),
            //   It.IsAny<decimal>()))
            //   .Throws<Exception>();
            var sut = new TaxService(new List<ITaxCalculatorService> { taxCalculatorServiceMock.Object });

            //Act                       
            var response = await sut.CalculateTaxForOrder(
                VALID_CUSTOMER_ID,
                VALID_ZIP_CODE,
                VALID_STATE,
                VALID_ZIP_CODE,
                VALID_STATE,
                VALID_ORDER_AMOUNT,
                VALID_ORDER_SHIPPING_AMOUNT);

            //Assert
            response.Should().NotBeNull();
            response.Error.Should().BeTrue();
            response.Message.Should().Be(TAX_CALCULATOR_NOT_AVAILABLE);
        }

        #endregion Calculate Taxes
    }
}
